﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Comp6211_A2
{
    class BubbleSortTiming
    {
        public double BubbleSort(float[] arr)
        {
            Stopwatch timer = Stopwatch.StartNew();
            float tmp;

            // For each element in array
            for (int i = 0; i < arr.Length; i++) {

                // Pass through array keeping smaller values to the left
                for (int j = 0; j < arr.Length - 1; j++) {

                    if (arr[j] > arr[j + 1]) {
                        tmp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = tmp;
                    }
                }
            }
            timer.Stop();
            return timer.Elapsed.TotalMilliseconds;
        }

        public double ImprovedBubbleSort(float[] arr) {
            Stopwatch timer = Stopwatch.StartNew();
            float tmp;
            bool hasSwapped;
            int numComparisons = arr.Length - 1;

            for (int i = 0; i < arr.Length; i++) {
                hasSwapped = false;

                for (int j = 0; j < numComparisons; j++) {

                    if (arr[j] > arr[j + 1]) {
                        hasSwapped = true;
                        tmp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = tmp;
                    }

                }
                // Larger values 'float/bubble' to the right/top of the array, 
                // we know the largest 'j * passes' elements have been found so we don't need to compare against them
                numComparisons = numComparisons - 1;
                // If we perform no swaps in a pass, the array is already sorted
                if (!hasSwapped) break;
            }
            timer.Stop();
            return timer.Elapsed.TotalMilliseconds;
        }
    }
}
