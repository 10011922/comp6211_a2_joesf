﻿using System;
using System.Collections.Generic;
//using System.Linq;


namespace Comp6211_A2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Application vars
            String dateTimeDataPath = "../../../../Data/DateTime_Data.txt"; // FilePaths up the top so you don't have to search for them =]
            String moistureDataPath = "../../../../Data/Moisture_Data.txt";
            String menu = "";
            bool run = true;
            int result = 0;

            // Data vars
            float[] rawMoisture = ImportDataNotNull<float>(moistureDataPath);
            float[] sortedMoisture;
            String[] rawDateTime;
            float minMoisture = 0.0f, maxMoisture = 0.0f, avgMoisture = 0.0f;

            // Make menu
            menu += " ||-----------------------------------------------------------------------------------||\n";
            menu += " ||   __        __   _______   ___       _______   _______   ____    ____   _______   ||\n";
            menu += " ||  |  |  __  |  | |       | |   |     |       | |       | |    |__|    | |       |  ||\n";
            menu += " ||  |  |_|  |_|  | |    ___| |   |     |    ___| |   _   | |            | |    ___|  ||\n";
            menu += " ||  |            | |   |___  |   |     |   |     |  | |  | |            | |   |___   ||\n";
            menu += " ||  |            | |    ___| |   |___  |   |___  |  |_|  | |   _    _   | |    ___|  ||\n";
            menu += " ||  |     __     | |   |___  |       | |       | |       | |  | |__| |  | |   |___   ||\n";
            menu += " ||  |____|  |____| |_______| |_______| |_______| |_______| |__|      |__| |_______|  ||\n";
            menu += " ||                                                                                   ||\n";
            menu += " || - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ||\n";
            menu += " ||  0 <-- View un-sorted moisture data                                               ||\n";
            menu += " || - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ||\n";
            menu += " ||  1 <-- Display 'n' highest moisture levels                                        ||\n";
            menu += " || - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ||\n";
            menu += " ||  2 <-- Use selection sort to display moisture levels in ascending order           ||\n";
            menu += " || - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ||\n";
            menu += " ||  3 <-- Find the date and time of minimum, maximum, and average moisture levels    ||\n";
            menu += " || - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ||\n";
            menu += " ||  4 <-- Benchmark naive bubble sort against optimized bubble sort                  ||\n";
            menu += " || - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ||\n";
            menu += " ||  5 <-- Exit program                                                               ||\n";
            menu += " || - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ||\n";
            menu += " || Please enter a valid menu number and prese <ENTER>                                ||\n";
            menu += " ||-----------------------------------------------------------------------------------||\n";

            while (run) {
                Console.Write(menu);
                result = GetUserInput(0, 6);
                Console.Clear();
                switch (result) {
                    case 0:
                        Console.WriteLine("Unsorted moisture levels: ");
                        PrintArray(rawMoisture);
                        Console.WriteLine("Press <ENTER> to return to the main menu...");
                        Console.ReadKey();
                        break;

                    case 1:
                        Console.WriteLine("Enter the amount of values to find");
                        PrintArray(FindMaximums(rawMoisture, GetUserInput(1, rawMoisture.Length)));
                        Console.WriteLine("Press <ENTER> to return to the main menu...");
                        Console.ReadKey();
                        break;

                    case 2:
                        // Print unsorted data
                        Console.WriteLine("Unsorted moisture levels: ");
                        PrintArray(rawMoisture);

                        // Generate data for output to user
                        Console.WriteLine("Press <ENTER> to sort moisture levels...");
                        Console.ReadKey(true);
                        sortedMoisture = SelectionSort(rawMoisture);                // Sort array
                        minMoisture = sortedMoisture[0];                            // Because the array is sorted, we know where min & max is
                        maxMoisture = sortedMoisture[sortedMoisture.Length - 1];

                        // Find average & round to two decimal places
                        // Originally I did this while sorting, but it feels wrong doing something other than sorting inside a sorting method...
                        for (int i = 0; i < sortedMoisture.Length; i++) { avgMoisture += sortedMoisture[i]; }
                        avgMoisture /= sortedMoisture.Length;
                        avgMoisture = (float)Math.Truncate(avgMoisture * 100.0) / 100.0f;

                        // Output sorted array, minimum, maximum & average.
                        Console.WriteLine("Sorted moisture levels: ");
                        PrintArray(sortedMoisture);
                        Console.Write("Minimum moisture level: " + minMoisture + "\nMaximum moisture level: " + maxMoisture + "\nAverage moisture level: " + avgMoisture + "\n\n");
                        Console.WriteLine("Press <ENTER> to return to the main menu...");
                        Console.ReadKey();
                        break;

                    case 3:
                        // Use linear search to find the index of min/max/avg in raw moisture data
                        int minIDX = LinearSearch(rawMoisture, minMoisture), maxIDX = LinearSearch(rawMoisture, maxMoisture), avgIDX = LinearSearch(rawMoisture, avgMoisture);

                        // Import dateTime data & compare arrays
                        rawDateTime = ImportDataNotNull<String>(dateTimeDataPath);
                        CompareArrays(minIDX, maxIDX, avgIDX, rawMoisture, rawDateTime);

                        Console.WriteLine("Press <ENTER> to return to the main menu...");
                        Console.ReadKey();
                        break;

                    case 4:
                        float[] tmpArr = rawMoisture;
                        BubbleSortTiming BST = new BubbleSortTiming();

                        // !Perform warmups! - The first sort will always be slower, ideally we'd also perform many reps
                        BST.BubbleSort(tmpArr);

                        tmpArr = rawMoisture;
                        Console.WriteLine("Naive bubblesort completion time : " + BST.BubbleSort(tmpArr) + "ms");

                        tmpArr = rawMoisture;
                        Console.WriteLine("Improved bubblesort completion time : " + BST.ImprovedBubbleSort(tmpArr) + "ms");

                        Console.WriteLine("Press <ENTER> to return to the main menu...");
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 5:
                        run = false;
                        break;
                }
                Console.Clear();
            }
        }

        /****               IMPORT DATA             ****/
        // Generic function that reads a file and converts it's content to type<T>. It also removes all null lines.
        public static T[] ImportDataNotNull<T>(String filePath) {
            int numLines = 0, idx = 0;
            String[] rawData = System.IO.File.ReadAllLines(filePath);
            bool[] validLines = new bool[rawData.Length];

            // We need to know how many && which lines are valid (i.e. not NULL, like in DateTime.txt)
            for (int i = 0; i < rawData.Length; i++) {
                if (rawData[i] != "") {
                    numLines++;
                    validLines[i] = true;
                }
                else { validLines[i] = false; }
            }

            // Now that we have the valid lines & know how many valid lines there are we can actually make the array
            T[] cleanData = new T[numLines];
            for (int i = 0; i < rawData.Length; i++) {
                if (validLines[i] == true) {
                    cleanData[idx] = (T)Convert.ChangeType(rawData[i], typeof(T));
                    idx++;
                }
            }
            return cleanData;
        }

        /****               SORTING ALGORITHMS              ****/
        // Sorts data in ascending order
        public static float[] SelectionSort(float[] uArr) {
            float[] sorted = uArr;
            float tmp = 0.0f;
            int min = 0;

            // For each element in array
            for (int i = 0; i < sorted.Length - 1; i++) {

                // Find smallest unsorted value, assume it is the first element
                min = i;
                for (int j = i + 1; j < sorted.Length; j++) {
                    if (sorted[j] < sorted[min]) { min = j; }
                }
                // Swap minimum value with the first element
                if (min != i) {
                    tmp = sorted[i];
                    sorted[i] = sorted[min];
                    sorted[min] = tmp;
                }
            }
            return sorted;
        }

        /****             SEARCH METHODS              ****/
        // Gehhh c# generics suck, c++ templates FTW -> https://msdn.microsoft.com/en-us/library/y097fkab.aspx?f=255&MSPPError=-2147217396
        public static float[] FindMaximums(float[] arr, int num) {
            float[] maximums = new float[num];
            int sIDX = 0;                           // Index of smallest value in maximums array

            // For each element in the data array
            for (int i = 0; i < arr.Length; i++) {
                // If element of data array is greater than the smallest element of maximums array
                if (arr[i] > maximums[sIDX]) {
                    // Input new maximum to maximums array
                    maximums[sIDX] = arr[i];
                    // Loop through each element of maximums array and find them smallest value, assign index of smallest value
                    int smallest = 0;
                    for (int j = 0; j < num; j++) { if (maximums[j] < maximums[smallest]) { smallest = j; } }
                    sIDX = smallest;
                }
            }
            return maximums;
        }

        // I'd much rather return an array of all indices that had the same value,
        // but we weren't asked for it so I'm going to be strong and not do it.
        // Did I mention generics suck, 'Operator == cannot be applied to operands of type T and T'
        public static int LinearSearch(float[] arr, float val) {
            int index = 0;
            // Loop through the array until we find the value we're looking for
            for (int i = 0; i < arr.Length; i++) { if (arr[i] == val) { index = i; } }
            return index;
        }
        /****               PRINT METHODS               ****/
        // Generic PrintArray
        public static void PrintArray<T>(T[] arr) {
            String o = "";
            foreach (T element in arr) { o += element.ToString() + "\n"; }
            Console.Write(o + "\n");
        }

        public static void CompareArrays<T, A>(int minID, int maxID, int avgID, T[] arr1, A[] arr2)        {
            Console.WriteLine("Minimum moisture level (" + arr1[minID] + ") ===> " + arr2[minID]);
            Console.WriteLine("Maximum moisture level (" + arr1[maxID] + ") ===> " + arr2[maxID]);
            Console.WriteLine("Average moisture level (" + arr1[avgID] + ") ===> " + arr2[avgID]);
            Console.WriteLine();
        }

        /****               MISC METHODS                ****/
        // Used to sanitize user input
        public static int GetUserInput(int min, int max) {
            int res = -1;
            bool isInt = false;

            while (!isInt) {
                isInt = int.TryParse(Console.ReadLine(), out res);
                if (!isInt || res < min || res > max) { Console.WriteLine("Please enter an integer value between " + min + " and " + max + "\n"); }
            }
            return res;
        }
    }
}
